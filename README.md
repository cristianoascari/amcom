# AMcom

Teste de desenvolvimento.

## Observações

  - Procurei utilizar apenas recursos do Angular e do PrimeNG, mas utilizei um componente de máscara a parte para o campo de preço do formulário.
  - Não estava na especificação, mas criei o aplicativo com suporte a internacionalização. Também criei uma página 'not found' padrão. Se tivesse mais tempo livre (achei que teria bastante quando iniciei o teste!) teria feito algumas outras melhorias, como busca, paginação e ordenação por coluna na página da listagem de itens.
  - Os componentes (arquivos component.ts) possuem algumas observações nas primeiras linhas.
  - Se tiverem alguma dúvida podem entrar em contato.

## Tecnologias utilizadas

Para o desenvolvimento do aplicativo utilizei as seguintes ferramentas e tecnologias:

  - Linux CentOS 7
  - Node 8.11.3
  - NPM 6.1.0
  - Angular 6.0.7
  - Angular CLI 6.0.8
  - TypeScript 2.7.2
  - RXJS 6.2.1
  - Webpack 4.8.3
  - PrimeNG 6.0.0
  - HTML5, CSS3, Sass e JavaScript (ES6)
  - Visual Studio Code for Linux 1.24.1

## Executando a aplicação

Para executar a versão de desenvolvimento (código aberto, permite inspecionar) deve-se ter o Node.js instalado e seguir os seguintes passos:

  - Clone o repositório na máquina local.
  - Acesse o diretório 'AMcom' (mesmo nível do arquivo 'package.json') e execute 'npm install' para instalar as dependências necessárias.
  - Execute -ng serve --open' para executar na porta padrão 4200. O parâmetro '--open' deve abrir automaticamente o navegador padrão apontando para 'http://localhost:4200'.
  - Caso queira rodar em uma porta diferente (4300, por exemplo), insira o parâmetro '--port:4300' ao 'ng serve' (Ex.: 'ng serve --port=4300 --open).

Para executar a versão de produção (minimiza e concatena arquivos, remove comentários e códigos ou dependências não utilizados):

  - Para compilar para um ambiente de produção execute 'ng build --prod'. O código gerado será armazenado no diretório 'dist'.
  - Se quiser executar o código em ambiente de produção adicione o parâmetro '--prod' ao comando 'ng serve'.