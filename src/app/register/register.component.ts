/**
 * ===================================================================================
 * OBSERVAÇÕES:
 * - Componente responsável pela inclusão e edição de items.
 * - Procurei utilizar apenas recursos do PrimeNG.
 * - O campo 'nome' é do tipo texto, só aceita letras e possui limite de 50 caracteres.
 * - O select de 'unidade de medidas' obriga a selecionar um item (o último utilizado_
 *   vem como padrão), mas se a quantidade não for informada não é considerado.
 * - 'Quantidade' é um campo opcional. Utilizei a máscara fornecida pelo PrimeNG para_
 *   seguir as regras da especificação.
 * - 'Preço' é obrigatório e utiliza o component 'currencyMask'. Já tinha utilizado e_
 *   achei mais fácil implementar o que foi exigindo utilizando-o, porém precisei_
 *   deixar o '[(ngModel)]' entre os atributos e isto dispara um warning no console_
 *   dizendo que o ngModel não será mais permitido nesta situação a partir da versão_
 *   7 do Angular.
 * - 'Produto perecível' é um checkbox booleano do PrimeNG. Na especificação está como_
 *   obrigatório, o que me fez pensar que talvez devesse ter utilizado um seletor do_
 *   tipo switch, mas fiz como está especificado.
 * - 'Data de validade' só é obrigatório se o checkbox de produto perecível estiver_
 *   selecionado.
 * - Para 'Data de fabricação' eu achei interessante impedir a inserção de datas_
 *   futuras. Valida se a data informada não é posterior a data de validade.
 * - Sobre as validações: há uma básica utilizando recursos do PrimeNG e outra que é_
 *   realizada no service que salva os dados.
 * ===================================================================================
 */

// Angular modules.
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

// Third-party modules.
import { ConfirmationService } from 'primeng/api';
import { Message } from 'primeng/api';
import { SelectItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

// Enums.
import { EMeasure } from '_enums/measure.enum';

// Interfaces.
import { ICalendar } from '_interfaces/calendar.interface';
import { IItem } from '_interfaces/item.interface';

// Services.
import { DatetimeService } from '_services/datetime/datetime.service';
import { LanguageService } from '_services/language/language.service';
import { MeasureService } from '_services/measure/measure.service';
import { StorageService } from '_services/storage/storage.service';

// On enter page animation.
const enterAnimation =

  trigger(
    'enterAnimation', [
      transition(':enter', [
        style({opacity: 0, 'margin-top': '-100%'}),
        animate('100ms ease-in-out', style({opacity: 0.3, 'margin-top': '-50%'})),
        animate('400ms ease-in-out', style({opacity: 1, 'margin-top': 0})),
      ]),
      transition(':leave', [
        style({top: '50%', opacity: 1}),
        animate('300ms', style({top: '0', opacity: 0}))
      ])
    ]
  );

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [ConfirmationService],
  animations: [enterAnimation]
})
export class RegisterComponent implements OnInit {

  // Language.
  public langSubscription: Subscription;

  // Item related.
  public item: IItem = {} as IItem;
  public itemId: number;
  public measureUnits: SelectItem[] = [];
  public currentMeasure: string = null;

  // Form.
  public itemForm: FormGroup;
  public growlMsgs: Message[] = [];

  // Measure.
  public measureMask: string = '***9';
  public measureSlotChar: string = '';

  // Calendar related.
  public calendarLables: ICalendar = {} as ICalendar;
  public today: Date = new Date();
  public dateFormat: string = 'dd/mm/yy';

  // Filters.
  public filterQtde: RegExp = /^[0-9]+(\.[0-9]{1,2})?$/;

  // Status.
  public isLoading: boolean = false;
  public editMode: boolean = false;
  public expDateRequired: boolean = false;
  public submitted: boolean = false;

  // Constructor method.
  constructor(
    protected datetimeService: DatetimeService,
    protected confirmationService: ConfirmationService,
    protected fb: FormBuilder,
    protected languageService: LanguageService,
    protected measureService: MeasureService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected storageService: StorageService,
    protected translate: TranslateService
  ) {

    // Loader.
    this.isLoading = true;

    // Get all translated values.
    this.getTranslatedValues();

    // Build form group.
    this.buildForm();

    // Get item ID (edit mode).
    this.route.params.subscribe(params => {
      if (params && params['id']) {
        this.isLoading = true;
        this.editMode = true;
        this.itemId = parseInt(params['id'], 10);
        this.getItem(this.itemId);
      }
    });

    // Force reload.
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/up') {
          this.router.navigate(['add']);
        }
      }
    });

  }

  // On init.
  public ngOnInit(): void {

    // If language changes get all translated values.
    this.langSubscription = this.languageService.languageChanges.subscribe(
      newLang => {
        if (newLang) {
          this.isLoading = true;
          this.getTranslatedValues();
        }
      }
    );

  }

  // Get all translated values.
  protected getTranslatedValues(): void {

    // Get calendars labels.
    this.getCalendarLabels();

    // Get units of measure.
    this.getUnitsOfMeasure(this.itemId ? false : true);

    // Get date format.
    this.dateFormat = this.datetimeService.getDateFormat(this.translate.currentLang);

  }

  // Build form items.
  private buildForm(): void {

    this.itemForm = new FormGroup({
      'expirationDate': new FormControl(''),
      'manufacturingDate': new FormControl('', Validators.required),
      'measure': new FormControl(''),
      'name': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50)])),
      'perishable': new FormControl(''),
      'price': new FormControl('', Validators.required),
      'quantity': new FormControl('')
    });

  }

  // Get item's data.
  protected getItem(id?: number): void {

    this.item = this.storageService.getItem(id || this.itemId);
    this.isLoading = false;

  }

  // Get calendars labels.
  protected getCalendarLabels(): void {

    this.calendarLables = this.languageService.getCalendarLabels();

  }

  // Get units of measure.
  protected getUnitsOfMeasure(lastStep: boolean = true): void {

    // Reset list.
    this.measureUnits = [];

    // Get updated items.
    this.measureService.buildMeasuresList().then(result => {

      // Assign items.
      result.forEach(item => { this.measureUnits.push({ label: item.Full, value: item.Short }); });

      // Sort by name and get the default one.
      if (this.measureUnits.length > 0) {
        this.measureUnits.sort(function(a, b) {return (a.label > b.label) ? 1 : ((b.label > a.label) ? -1 : 0); });
        // this.currentMeasure = localStorage.getItem('measure') || this.measureUnits[0].value;
        this.currentMeasure = this.measureUnits[0].value;
        this.changeMeasure();
      }

      // Loader.
      if (lastStep || 1 === 1) {
        this.isLoading = false;
      }

    });

  }

  // Change active unit of measure.
  public changeMeasure(unit?: any): void {

    this.currentMeasure = unit || this.currentMeasure;
    localStorage.setItem('measure', this.currentMeasure);

    this.measureMask = this.currentMeasure === 'un' ? '***9' : '9,999';
    this.measureSlotChar = this.currentMeasure === 'un' ? '' : '0,000';

  }

  // Check expiration date.
  public checkExpirationDate(): void {

    const dt: Date = new Date(this.itemForm.controls['expirationDate'].value);

    if (dt) {

      // Get dates to compare.
      const today: Date = new Date();
      const exp: Date = new Date(dt);

      // Sync time.
      today.setHours(0, 0, 0, 0);
      exp.setHours(0, 0, 0, 0);

      // Compare.
      if (exp < today) {
        alert(this.translate.instant('MESSAGES.VALIDATION.EXPIRED_PRODUCT'));
      }

      // Check manufacturing date.
      this.checkManufacturingDate();

    }

  }

  // Check anufacturing date.
  public checkManufacturingDate(): void {

    if (this.item.expirationDate && this.item.manufacturingDate) {

      // Get dates to compare.
      const exp: Date = new Date(this.item.expirationDate);
      const man: Date = new Date(this.item.manufacturingDate);

      // Sync time.
      exp.setHours(0, 0, 0, 0);
      man.setHours(0, 0, 0, 0);

      // Compare.
      if (exp < man) {
        alert(this.translate.instant('MESSAGES.VALIDATION.MANUFACTURING_DATE_WRONG'));
      }

    }

  }

  // Cancel add/edit operation.
  public cancel(): void {

    const message: string = 'MESSAGES.CONFIRM.ARE_YOU_SURE';

    this.translate.get(message).subscribe(res => {

      this.confirmationService.confirm({
        message: res,
        accept: () => { this.router.navigate(['/']); },
        reject: () => {}
      });

    });

  }

  // Save item.
  public save(data: any): void {

    // Reset validators.
    let isOK: boolean = true;
    this.growlMsgs = [];
    this.expDateRequired = false;

    // Get data.
    this.item = data;

    // Verify unit of measure.
    switch (this.currentMeasure) {

      case 'kg':
        this.item.measure = EMeasure.KILOGRAM;
        break;

      case 'lt':
        this.item.measure = EMeasure.LITER;
        break;

      case 'un':
        this.item.measure = EMeasure.UNIT;
        break;

    }

    // Expiration date required?
    if (this.item.perishable && !this.item.expirationDate) {
      isOK = false;
      this.expDateRequired = true;
    }

    // Try to save.
    if (isOK) {

      this.storageService.saveItem(this.item).then(
        res => {
          this.item = {} as IItem;
          this.router.navigate([this.itemId ? '/' : '/up']);
        },
        err => {
          this.growlMsgs.push({
            severity: 'error',
            summary: this.translate.instant('MESSAGES.ERROR.FORM_ERROR'),
            detail: err.message
          });
        }
      );

    }

  }

}
