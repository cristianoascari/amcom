/**
 * ===================================================================================
 * OBSERVAÇÕES:
 * - Component responsável pela listagem dos itens cadastrados.
 * - Se não há itens exibe apenas uma mensagem e um link para adicionar novos.
 * - A tabela é um componente do PrimeNG.
 * - Em resoluções menores algumas colunas são omitidas.
 * - Poderia de criado um filtro e possibilitado a ordenação por colunas, mas não_
 *   tive muito tempo disponível.
 * ===================================================================================
 */

// Angular modules.
import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Third-party modules.
import { ConfirmationService } from 'primeng/api';
import { Message } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

// Enums.
import { EMeasureShort } from '_enums/measure.enum';

// Interfaces.
import { IItem } from '_interfaces/item.interface';

// Services.
import { DatetimeService } from '_services/datetime/datetime.service';
import { MeasureService } from '_services/measure/measure.service';
import { StorageService } from '_services/storage/storage.service';

// On enter page animation.
const enterAnimation =

  trigger(
    'enterAnimation', [
      transition(':enter', [
        style({opacity: 0, 'margin-top': '-100%'}),
        animate('100ms ease-in-out', style({opacity: 0.3, 'margin-top': '-50%'})),
        animate('400ms ease-in-out', style({opacity: 1, 'margin-top': 0})),
      ]),
      transition(':leave', [
        style({top: '50%', opacity: 1}),
        animate('300ms', style({top: '0', opacity: 0}))
      ])
    ]
  );

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss'],
  providers: [ConfirmationService],
  animations: [enterAnimation]
})
export class ListingComponent implements OnInit {

  // Items related.
  public items: IItem[];

  // Status.
  public isLoading: boolean = false;
  public growlMsgs: Message[] = [];

  // Constructor method.
  constructor(
    protected confirmationService: ConfirmationService,
    protected datetimeService: DatetimeService,
    protected measureService: MeasureService,
    protected router: Router,
    protected storageService: StorageService,
    protected translate: TranslateService
  ) { }

  // On init
  ngOnInit() {

    // Get stored items.
    this.getItems();

  }

  // Get stored items.
  protected getItems(): void {

    this.items = this.storageService.getItems();

  }

  // Get formatted quantity.
  public getQuantityText(value: number, measure: string): string {

    return value ? value + this.translate.instant('UNITS_OF_MEASURE.SHORT.' + EMeasureShort[measure]) : '--';

  }

  // Get formatted date.
  public getDateText(date: Date): string {

    return date ? this.datetimeService.getTranslatedDate(date, this.translate.currentLang) : '--';

  }

  // Edit an item.
  public editItem(itemId: number): void {

    this.router.navigate(['/edit', itemId]);

  }

  // Remove an item.
  public removeItem(itemId: number): void {

    const message: string = 'MESSAGES.CONFIRM.ARE_YOU_SURE';

    this.translate.get(message).subscribe(result => {

      this.confirmationService.confirm({
        message: result,
        accept: () => {

          this.storageService.remove(itemId).then(
            res => {

              this.growlMsgs.push({
                severity: 'success',
                summary: this.translate.instant('MESSAGES.SUCCESS.SUCCESS'),
                detail: this.translate.instant('MESSAGES.SUCCESS.ITEM_REMOVED')
              });

              // Get updated items.
              this.getItems();

            },
            err => {
              this.growlMsgs.push({
                severity: 'error',
                summary: this.translate.instant('MESSAGES.ERROR.ERROR'),
                detail: err.message
              });
            }
          );

        },
        reject: () => {}
      });

    });

  }

  // Add a new item.
  public addNew(): void {

    this.router.navigate(['/add']);

  }

}
