/**
 * ===================================================================================
 * OBSERVAÇÕES:
 * - Não estava na especificação, mas criei para tratar URLs inválidas (poderia ter_
 *   simplesmente redirecionado para a home).
 * ===================================================================================
 */

// Angular modules.
import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Third-party modules.
import { TranslateService } from '@ngx-translate/core';

// On enter page animation.
const enterAnimation =

  trigger(
    'enterAnimation', [
      transition(':enter', [
        style({opacity: 0, 'margin-top': '-100%'}),
        animate('100ms ease-in-out', style({opacity: 0.3, 'margin-top': '-50%'})),
        animate('400ms ease-in-out', style({opacity: 1, 'margin-top': 0})),
      ]),
      transition(':leave', [
        style({top: '50%', opacity: 1}),
        animate('300ms', style({top: '0', opacity: 0}))
      ])
    ]
  );

@Component({
  selector: 'app-notfound',
  templateUrl: './notfound.component.html',
  styleUrls: ['./notfound.component.scss'],
  animations: [enterAnimation]
})
export class NotfoundComponent implements OnInit {

  // Constructor method.
  constructor(
    protected router: Router,
    protected translate: TranslateService
  ) {}

  // On init
  ngOnInit() {}

  // Go to main page.
  public goToHome(): void {

    this.router.navigate(['/']);

  }

}
