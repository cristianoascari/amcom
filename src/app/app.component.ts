/**
 * ===================================================================================
 * OBSERVAÇÕES:
 * - Arquivo principal, configura o idioma padrão, controla o menu principal e_
 *   serve de container para as outras páginas.
 * - Geralmente crio um componente adicional para servir de container (main ou home),_
 *   mas como o projeto é pequeno resolvi utilizar este mesmo.
 * ===================================================================================
 */

// Angular modules.
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Third-party modules.
import { SelectItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

// Interfaces.
import { IMeasure } from '_interfaces/measure.interface';

// Services.
import { LanguageService } from '_services/language/language.service';
import { MeasureService } from '_services/measure/measure.service';
import { MenuService } from '_services/menu/menu.service';
import { StorageService } from '_services/storage/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  // Menu.
  public showMenu: boolean = false;
  public menuOptions: SelectItem[] = [];
  public menuActive: SelectItem = null;

  // Units of measure.
  public measures: IMeasure[] = [];

  // Languages list.
  public languages: SelectItem[] = [];
  public currentLanguage: string = null;

  // Status.
  public isLoading: boolean = true;

  // Constructor method.
  constructor(
    protected languageService: LanguageService,
    protected measureService: MeasureService,
    protected menuService: MenuService,
    private router: Router,
    protected storageService: StorageService,
    protected translate: TranslateService
  ) {

    // Get (or set) current language.
    this.currentLanguage = this.languageService.getLanguage();

    // Build languages list.
    this.languages = this.languageService.getList();

    // Set default language.
    translate.setDefaultLang(this.currentLanguage);

  }

  // On init.
  public ngOnInit(): void {

    // Get translated menu items.
    this.getMenuItems().then(result => {

      // Loader.
      this.isLoading = false;

    });

  }

  // Change current language.
  public changeLanguage(lang: any): void {

    this.languageService.setLanguage(lang).then(result => {

      // Update texts.
      this.getMenuItems();

    });

  }

  // Get translated menu items.
  private getMenuItems(): Promise<any> {

    // Create a promise.
    const promise = new Promise((resolve, reject) => {

      this.menuService.buildOptionsMenu().then(result => {

        this.menuOptions = result;
        this.menuActive = this.menuActive || this.menuOptions[1];

        resolve();

      });

    });

    // Result.
    return promise;

  }

  // Toggle main menu visibility.
  public toggleMenu(): void {

    this.showMenu = !this.showMenu;

  }

  // Change current page.
  public changePage(menu: SelectItem): void {

    this.router.navigate([menu]);

  }

}
