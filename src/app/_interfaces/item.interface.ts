// Enums.
import { EMeasure } from '_enums/measure.enum';

// Interface for an item.
export interface IItem {
  id?: number;
  name: string;
  measure: EMeasure;
  quantity?: number;
  price: number;
  perishable: boolean;
  expirationDate?: Date;
  manufacturingDate: Date;
}
