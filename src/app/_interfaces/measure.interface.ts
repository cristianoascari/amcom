// Enums.
import { EMeasure, EMeasureShort } from '../_enums/measure.enum';

// Interface for units of measure.
export interface IMeasure {
  Item: EMeasure;
  Full: string;
  Short: string;
}
