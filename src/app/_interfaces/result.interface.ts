// Interface for operation results.
export interface IResult {
  success: boolean;
  message: string;
}
