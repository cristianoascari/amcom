// Interfaces for menu.
export interface IOptionsMenu {
  label: string;
  target?: string;
}
