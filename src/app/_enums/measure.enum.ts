// Enumerator for units of measure.
export enum EMeasure {
  LITER,
  KILOGRAM,
  UNIT
}

// Enumerator for abbreviated units.
export enum EMeasureShort {
  LT,
  KG,
  UN
}
