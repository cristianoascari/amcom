// Angular modules.
import { Injectable } from '@angular/core';

// Third-party modules.
import { TranslateService } from '@ngx-translate/core';

// Enums.
import { EMeasure, EMeasureShort } from '_enums/measure.enum';

// Interfaces.
import { IMeasure } from '_interfaces/measure.interface';

@Injectable({
  providedIn: 'root'
})
export class MeasureService {

  // Translation base paths.
  protected bUOMFull: string = 'UNITS_OF_MEASURE.FULL.';
  protected bUOMShort: string = 'UNITS_OF_MEASURE.SHORT.';
  // Constructor method.
  constructor(protected translate: TranslateService) { }

  // Build units of measure list.
  public buildMeasuresList(): Promise<any> {

    const promise = new Promise((resolve, reject) => {

      const measures: IMeasure[] = [];

      this.translate.get([
        this.bUOMFull + EMeasure[EMeasure.KILOGRAM],
        this.bUOMFull + EMeasure[EMeasure.LITER],
        this.bUOMFull + EMeasure[EMeasure.UNIT],
        this.bUOMShort + EMeasureShort[EMeasureShort.KG],
        this.bUOMShort + EMeasureShort[EMeasureShort.LT],
        this.bUOMShort + EMeasureShort[EMeasureShort.UN]
      ]).subscribe(res => {

        measures.push(<IMeasure>{
          Item: EMeasure.KILOGRAM,
          Full: res[this.bUOMFull + EMeasure[EMeasure.KILOGRAM]],
          Short: res[this.bUOMShort + EMeasureShort[EMeasureShort.KG]]
        });

        measures.push(<IMeasure>{
          Item: EMeasure.LITER,
          Full: res[this.bUOMFull + EMeasure[EMeasure.LITER]],
          Short: res[this.bUOMShort + EMeasureShort[EMeasureShort.LT]]
        });

        measures.push(<IMeasure>{
          Item: EMeasure.UNIT,
          Full: res[this.bUOMFull + EMeasure[EMeasure.UNIT]],
          Short: res[this.bUOMShort + EMeasureShort[EMeasureShort.UN]]
        });

        resolve(measures);

      });

    });

    return promise;

  }

  // Get a measure's short name.
  public getShortName(measure: number): Promise<any> {

    // Create a promise.
    const promise = new Promise((resolve, reject) => {

      this.translate.get('UNITS_OF_MEASURE.SHORT.' + EMeasureShort[measure]).subscribe(res => {
        resolve(res);
      });

    });

    // Result.
    return promise;

  }

}
