// Angular modules.
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

// Third-party modules.
import { SelectItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

// Interfaces.
import { ICalendar } from '_interfaces/calendar.interface';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  // Observable for language changes.
  private _changeLang = new BehaviorSubject<string>('');
  public languageChanges: Observable<string> = this._changeLang.asObservable();

  // Default language.
  private defaultLanguage: string = 'pt-BR';

  // Constructor method.
  constructor(protected translate: TranslateService) { }

  // Return current language.
  public getLanguage(): string {

    // Get stored language.
    let lang = localStorage.getItem('lang');

    // If none, set default.
    if (!lang) {
      lang = this.defaultLanguage;
      this.setLanguage(lang);
    }

    // Force Translate Service to use this lang.
    this.translate.use(lang);

    // Result.
    return lang;

  }

  // Set current language.
  public setLanguage(lang: string): Promise<any> {

    // Create a promise.
    const promise = new Promise((resolve, reject) => {

      // Format value.
      lang = lang.toLowerCase();

      // Current language.
      const current: string = this.getLanguage();

      // Final or default language.
      let final: string = this.defaultLanguage;

      // Change language if is passed a valid key.
      if (['en-us', 'enus', 'en'].indexOf(lang) > -1) {

        final = 'en-US';
        this.translate.use(final);

      // } else if (lang !== 'pt-BR' && ['pt-br', 'ptbr', 'pt'].indexOf(lang) > -1) {
      } else {

        this.translate.use(final);

      }

      // Save selected language.
      if (final !== current) {

        localStorage.setItem('lang', final);
        this._changeLang.next(final);

      }

      // OK.
      resolve();

    });

    // Result.
    return promise;

  }

  // Return languages list.
  public getList(): SelectItem[] {

    const languages: SelectItem[] = [];
    languages.push({ label: 'en-US', value: 'en-US' });
    languages.push({ label: 'pt-BR', value: 'pt-BR' });

    return languages;

  }

  // Return calendar labels.
  public getCalendarLabels(): ICalendar {

    /**
     * OBSERVAÇÃO:
     * Poderia ter criado as entradas nos arquivos de tradução. Fiz aqui para poupar tempo.
     */

    // Get current language.
    const lang: string = this.getLanguage();

    let labels: ICalendar = {} as ICalendar;

    if (lang === 'en-US') {

      labels = <ICalendar>{
        firstDayOfWeek: 0,
        dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        today: 'Today',
        clear: 'Clear'
      };

    } else {

      labels = <ICalendar>{
        firstDayOfWeek: 0,
        dayNames: ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dayNamesMin: ['Do', 'Se', 'Te', 'Qa', 'Qi', 'Sx', 'Sa'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'NOvembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        today: 'Hoje',
        clear: 'Limpar'
      };

    }

    // Result.
    return labels;

  }

}
