// Angular module.
import { Injectable } from '@angular/core';

// Third-party modules.
import { TranslateService } from '@ngx-translate/core';

// Interfaces.
import { IItem } from '_interfaces/item.interface';
import { IResult } from '_interfaces/result.interface';
import { EMeasure } from '_enums/measure.enum';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  // Stored key.
  public storedKey: string = 'items';

  // Translation base paths.
  public bMsgError: string = 'MESSAGES.ERROR.';
  public bMsgSuccess: string = 'MESSAGES.SUCCESS.';
  public bMsgValidation: string = 'MESSAGES.VALIDATION.';

  // Translated items.
  tItemAlreadyExists: string;
  tItemNotFound: string;
  tItemRemoved: string;
  tItemSaved: string;
  tItemUpdated: string;

  // Constructor method.
  constructor(protected translate: TranslateService) {

    // Get translated items.
    this.translate.get([
      this.bMsgError + 'ITEM_ALREADY_EXISTS',
      this.bMsgError + 'ITEM_NOT_FOUND',
      this.bMsgSuccess + 'ITEM_REMOVED',
      this.bMsgSuccess + 'ITEM_SAVED',
      this.bMsgSuccess + 'ITEM_UPDATED'
    ]).subscribe(res => {

      this.tItemAlreadyExists = res[this.bMsgError + 'ITEM_ALREADY_EXISTS'];
      this.tItemNotFound = res[this.bMsgError + 'ITEM_NOT_FOUND'];
      this.tItemRemoved = res[this.bMsgSuccess + 'ITEM_REMOVED'];
      this.tItemSaved = res[this.bMsgSuccess + 'ITEM_SAVED'];
      this.tItemUpdated = res[this.bMsgSuccess + 'ITEM_UPDATED'];

    });

  }

  // Return all items.
  public getItems(): IItem[] {

    let result: IItem[] = [];
    const items: string = localStorage.getItem('items');

    if (items) {
      result = JSON.parse(items);
    }

    return result;

  }

  // Return a specific item.
  public getItem(itemId: number): IItem {

    // Get current saved items.
    const savedItems: IItem[] = JSON.parse(localStorage.getItem(this.storedKey)) || [];

    // Verify if item already exists.
    const items: IItem[] = savedItems.filter(el => el.id === itemId);

    // Get item.
    let item: IItem = {} as IItem;
    if (items.length > 0) {
      item = items[0];
      item.expirationDate = item.expirationDate ? new Date(item.expirationDate) : null;
      item.manufacturingDate = new Date(item.manufacturingDate);
    }

    // Result.
    return item;

  }

  // Return an new item ID.
  public getItemId(): number {

    return Date.now();

  }

  // Save/update an item.
  public save(item: IItem): Promise<any> {

    // Create a promise.
    const promise = new Promise((resolve, reject) => {

      if (item.id) {

        this.updateItem(item).then(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );

      } else {

        this.saveItem(item).then(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );

      }

    });

    // Result.
    return promise;

  }

  // Update an item or register a new one.
  public saveItem(item: IItem): Promise<any> {

    // Create a promise.
    const promise = new Promise((resolve, reject) => {

      // Get current saved items.
      const savedItems: IItem[] = JSON.parse(localStorage.getItem(this.storedKey)) || [];

      // Verify if item already exists.
      const exists: IItem[] = item.id ? savedItems.filter(el => el.id === item.id) : [];

      // Save or update.
      if (exists.length > 0) {

        /*reject(<IResult>{
          success: false,
          message: this.tItemAlreadyExists
        });*/

        // Update an existing item.
        this.updateItem(item).then(
          success => resolve(success),
          error => reject(error)
        );

      } else {

        // Validate.
        this.validateItem(item).then(

          success => {

            // Get a new ID.
            item.id = this.getItemId();

            // Add new item.
            savedItems.push(item);

            // Save new item.
            localStorage.setItem(this.storedKey, JSON.stringify(savedItems));

            // Saved.
            resolve(<IResult>{
              success: true,
              message: this.tItemSaved
            });

          },
          error => {

            reject(error);

          }

        );

      }

    });

    // Return result.
    return promise;

  }

  // Update an item.
  private updateItem(item: IItem): Promise<any> {

    // Create a promise.
    const promise = new Promise((resolve, reject) => {

      // Get current saved items.
      const savedItems: IItem[] = JSON.parse(localStorage.getItem(this.storedKey)) || [];

      // Get saved item.
      const savedItem: IItem[] = savedItems.filter(el => el.id === item.id);

      // Save or reject.
      if (savedItem.length > 0) {

        this.validateItem(item).then(

          success => {

            // Update item at list.
            const updated: IItem[] = savedItems.map(el => {
              return el.id === item.id ? item : el;
            });

            // Save new item.
            localStorage.setItem(this.storedKey, JSON.stringify(updated));

            // Updated.
            resolve(<IResult>{
              success: true,
              message: this.tItemUpdated
            });

          },
          error => {

            reject(error);

          }

        );

      } else {

        reject(<IResult>{
          success: false,
          message: this.tItemNotFound
        });

      }

    });

    // Return result.
    return promise;

  }

  // Validate an item.
  public validateItem(item: IItem): Promise<any> {

    // Create a promise.
    const promise = new Promise((resolve, reject) => {

      // Hold base path of translated strings.
      const basePath: string = this.bMsgValidation;

      // Get validation error texts.
      this.translate.get([
        basePath + 'MANUFACTURING_DATE_WRONG',
        basePath + 'NAME_EXCEED_LENGTH',
        basePath + 'PRICE_IS_NOT_NUMBER',
        basePath + 'QUANTITY_IS_NOT_NUMBER',
        basePath + 'REQUIRED_EXPIRATION_DATE',
        basePath + 'REQUIRED_MANUFACTURING_DATE',
        basePath + 'REQUIRED_NAME',
        basePath + 'REQUIRED_PRICE',
        basePath + 'REQUIRED_UNITMEASURE',
        basePath + 'TYPE_ONLY_LETTERS'
      ]).subscribe(res => {

        let hasError: boolean = false;
        let errorMessage: string = null;

        // Have a name?
        if (!item.name) {

          hasError = true;
          errorMessage = res[basePath + 'REQUIRED_NAME'];

        // Name size between 1 and 50?
        } else if (item.name.length > 50) {

          hasError = true;
          errorMessage = res[basePath + 'NAME_EXCEED_LENGTH'];

        // Has selected an unit of measure?
        } else if (!item.measure && item.measure !== 0) {

          hasError = true;
          errorMessage = res[basePath + 'REQUIRED_UNITMEASURE'];

        // Have a price?
        } else if (!item.price) {

          hasError = true;
          errorMessage = res[basePath + 'REQUIRED_PRICE'];

        // Price value is a number?
        } else if (item.price && isNaN(item.price)) {

          hasError = true;
          errorMessage = res[basePath + 'PRICE_IS_NOT_NUMBER'];

        } else if (item.expirationDate && item.manufacturingDate && item.manufacturingDate > item.expirationDate) {

          hasError = true;
          errorMessage = res[basePath + 'MANUFACTURING_DATE_WRONG'];

        }

        if (hasError) {

          reject(<IResult>{
            success: false,
            message: errorMessage
          });

        } else {

          resolve(<IResult>{
            success: true,
            message: null
          });

        }

      });

    });

    // Result.
    return promise;

  }

  // Delete an item.
  public remove(itemId: number): Promise<any> {

    // Create a promise.
    const promise = new Promise((resolve, reject) => {

      // Get current saved items.
      const savedItems: IItem[] = JSON.parse(localStorage.getItem(this.storedKey)) || [];

      // Get saved item.
      const savedItem: IItem[] = savedItems.filter(el => el.id === itemId);

      // Save or reject.
      if (savedItem.length > 0) {

        // Update item at list.
        const updated: IItem[] = savedItems.filter(el => el.id !== itemId);

        // Save new item.
        localStorage.setItem(this.storedKey, JSON.stringify(updated));

        // Updated.
        resolve(<IResult>{
          success: true,
          message: this.tItemRemoved
        });

      } else {

        reject(<IResult>{
          success: false,
          message: this.tItemNotFound
        });

      }

    });

    // Result.
    return promise;

  }

}
