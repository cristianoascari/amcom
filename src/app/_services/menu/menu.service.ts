// Angular modules.
import { Injectable } from '@angular/core';

// Third-party modules.
import { TranslateService } from '@ngx-translate/core';

// Interfaces.
import { IOptionsMenu } from '_interfaces/menu.interface';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  // Constructor method.
  constructor(protected translate: TranslateService) { }

  // Build translated options menu items.
  public buildOptionsMenu(): Promise<any> {

    // Create a promise.
    const promise = new Promise((resolve, reject) => {

      this.translate.get([
        'MENU.LISTING',
        'MENU.REGISTER'
      ]).subscribe(res => {

        const menus: IOptionsMenu[] = [];
        menus.push(<IOptionsMenu>{ label: res['MENU.LISTING'], value: '/' });
        menus.push(<IOptionsMenu>{ label: res['MENU.REGISTER'], value: 'add' });

        resolve(menus);

      });

    });

    // Result.
    return promise;

  }

}
