// Angular methods.
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatetimeService {

  // Constructor method.
  constructor() { }

  // Return date format according to current language.
  public getDateFormat(lang: string): string {

    let format: string = 'dd/mm/yy';

    switch (lang) {

      case 'en-US':
        format = 'yy/mm/dd';
        break;

      default:
        format = 'dd/mm/yy';

    }

    return format;

  }

  // Return a translated date.
  public getTranslatedDate(date: Date, lang: string): string {

    date = new Date(date);
    let finalDate: string;

    const y: number = date.getFullYear();
    const m: number = date.getMonth();
    const d: number = date.getDate();

    if (lang === 'en-US') {

      finalDate = y.toString() + '/' + (m <= 9 ? '0' + m : m).toString() + '/' + (d <= 9 ? '0' + d : d).toString();

    } else {

      finalDate = (d <= 9 ? '0' + d : d).toString() + '/' + (m <= 9 ? '0' + m : m).toString() + '/' + y.toString();

    }

    return finalDate;

  }

}
